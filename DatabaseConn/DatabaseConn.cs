﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Data.SqlClient;

namespace DatabaseConn
{
    public partial class DatabaseConn : Form
    {

        private IDbConnection oConn;


        public DatabaseConn()
        {
            InitializeComponent();
             oConn = null;
        }

        public IDbConnection GetConnection()
        {
            return oConn;
        }

        private void PictureBox1_Click(object sender, EventArgs e)
        {

        }

        public string BuildConnectionString()
        {
            string sStr = "Server = " + txtServer.Text;

            sStr += ";Database = " + txtDB.Text;
            sStr += sStr + ";UID = " + txtUser.Text;
            sStr = sStr + ";PWD = " + txtPwd.Text;
            sStr = sStr + ";Trusted_Connection = " + radTrusted.Checked;
            return sStr;
        }

        public bool TestConnection(string sConnstr) 
        {
            bool bOk  = false;


            Cursor = Cursors.WaitCursor;

            try
            {
                if ( CreateConnection(sConnstr, ref oConn, "MSDASQL")) 
                    bOk = true;

            }
            catch( Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            Cursor = Cursors.Default;
            return bOk;
        }

        public bool CreateConnection(string sConnString,ref IDbConnection oConn, string sProvider) 
        {
            bool bOk = true;

            switch (sProvider.ToUpper())
            {
                case "MSDASQL":
                    oConn = new SqlConnection();
                    break;
                case "SQLOLEDB":
                    oConn = new OleDbConnection();
                    break;
                case "ORACLE":
                    // oConn = new OracleClient.OracleConnection();
                default:
                    oConn = new SqlConnection();
                    break;

            }
            try
            {
                oConn.ConnectionString = sConnString;
                oConn.Open();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                bOk = false;
            }

            return bOk;
        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            string sConnStr = BuildConnectionString();
            if (!TestConnection(sConnStr))
               this.DialogResult = DialogResult.Ignore;

        }

        private void radSQL_CheckedChanged(object sender, EventArgs e)
        {
            txtUser.Enabled = true;
            txtPwd.Enabled = true;
        }
    }
}
