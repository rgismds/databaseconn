﻿namespace DatabaseConn
{
    partial class DatabaseConn
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DatabaseConn));
            this.PictureBox1 = new System.Windows.Forms.PictureBox();
            this.Label3 = new System.Windows.Forms.Label();
            this.txtServer = new System.Windows.Forms.TextBox();
            this.txtDB = new System.Windows.Forms.TextBox();
            this.Label4 = new System.Windows.Forms.Label();
            this.radTrusted = new System.Windows.Forms.RadioButton();
            this.radSQL = new System.Windows.Forms.RadioButton();
            this.btnCancel = new System.Windows.Forms.Button();
            this.Label1 = new System.Windows.Forms.Label();
            this.btnTest = new System.Windows.Forms.Button();
            this.txtPwd = new System.Windows.Forms.TextBox();
            this.Label6 = new System.Windows.Forms.Label();
            this.txtUser = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // PictureBox1
            // 
            this.PictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("PictureBox1.Image")));
            this.PictureBox1.Location = new System.Drawing.Point(11, 36);
            this.PictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.PictureBox1.Name = "PictureBox1";
            this.PictureBox1.Size = new System.Drawing.Size(70, 69);
            this.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PictureBox1.TabIndex = 5;
            this.PictureBox1.TabStop = false;
            this.PictureBox1.Click += new System.EventHandler(this.PictureBox1_Click);
            // 
            // Label3
            // 
            this.Label3.AutoSize = true;
            this.Label3.Location = new System.Drawing.Point(109, 16);
            this.Label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label3.Name = "Label3";
            this.Label3.Size = new System.Drawing.Size(91, 17);
            this.Label3.TabIndex = 7;
            this.Label3.Text = "Server Name";
            // 
            // txtServer
            // 
            this.txtServer.Location = new System.Drawing.Point(113, 36);
            this.txtServer.Margin = new System.Windows.Forms.Padding(4);
            this.txtServer.Name = "txtServer";
            this.txtServer.Size = new System.Drawing.Size(395, 22);
            this.txtServer.TabIndex = 6;
            this.txtServer.Text = "EU-DB01\\UKSM";
            // 
            // txtDB
            // 
            this.txtDB.Location = new System.Drawing.Point(113, 92);
            this.txtDB.Margin = new System.Windows.Forms.Padding(4);
            this.txtDB.Name = "txtDB";
            this.txtDB.Size = new System.Drawing.Size(395, 22);
            this.txtDB.TabIndex = 8;
            this.txtDB.Text = "Sainsburys";
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Location = new System.Drawing.Point(109, 73);
            this.Label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(110, 17);
            this.Label4.TabIndex = 9;
            this.Label4.Text = "Database Name";
            // 
            // radTrusted
            // 
            this.radTrusted.AutoSize = true;
            this.radTrusted.Checked = true;
            this.radTrusted.Location = new System.Drawing.Point(113, 142);
            this.radTrusted.Margin = new System.Windows.Forms.Padding(4);
            this.radTrusted.Name = "radTrusted";
            this.radTrusted.Size = new System.Drawing.Size(153, 21);
            this.radTrusted.TabIndex = 10;
            this.radTrusted.TabStop = true;
            this.radTrusted.Text = "Trusted Connection";
            this.radTrusted.UseVisualStyleBackColor = true;
            // 
            // radSQL
            // 
            this.radSQL.AutoSize = true;
            this.radSQL.Location = new System.Drawing.Point(113, 170);
            this.radSQL.Margin = new System.Windows.Forms.Padding(4);
            this.radSQL.Name = "radSQL";
            this.radSQL.Size = new System.Drawing.Size(151, 21);
            this.radSQL.TabIndex = 11;
            this.radSQL.Text = "SQL Authentication";
            this.radSQL.UseVisualStyleBackColor = true;
            this.radSQL.CheckedChanged += new System.EventHandler(this.radSQL_CheckedChanged);
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(379, 327);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(100, 28);
            this.btnCancel.TabIndex = 28;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Location = new System.Drawing.Point(111, 264);
            this.Label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(69, 17);
            this.Label1.TabIndex = 30;
            this.Label1.Text = "Password";
            // 
            // btnTest
            // 
            this.btnTest.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.btnTest.Location = new System.Drawing.Point(246, 327);
            this.btnTest.Margin = new System.Windows.Forms.Padding(4);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(125, 28);
            this.btnTest.TabIndex = 27;
            this.btnTest.Text = "Test Connection";
            this.btnTest.UseVisualStyleBackColor = true;
            this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
            // 
            // txtPwd
            // 
            this.txtPwd.Enabled = false;
            this.txtPwd.Location = new System.Drawing.Point(115, 284);
            this.txtPwd.Margin = new System.Windows.Forms.Padding(4);
            this.txtPwd.Name = "txtPwd";
            this.txtPwd.PasswordChar = '*';
            this.txtPwd.Size = new System.Drawing.Size(363, 22);
            this.txtPwd.TabIndex = 26;
            // 
            // Label6
            // 
            this.Label6.AutoSize = true;
            this.Label6.Location = new System.Drawing.Point(111, 211);
            this.Label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Label6.Name = "Label6";
            this.Label6.Size = new System.Drawing.Size(79, 17);
            this.Label6.TabIndex = 29;
            this.Label6.Text = "User Name";
            // 
            // txtUser
            // 
            this.txtUser.Enabled = false;
            this.txtUser.Location = new System.Drawing.Point(115, 231);
            this.txtUser.Margin = new System.Windows.Forms.Padding(4);
            this.txtUser.Name = "txtUser";
            this.txtUser.Size = new System.Drawing.Size(363, 22);
            this.txtUser.TabIndex = 25;
            this.txtUser.Text = "Sainsburys";
            // 
            // DatabaseConn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(522, 369);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.Label1);
            this.Controls.Add(this.btnTest);
            this.Controls.Add(this.txtPwd);
            this.Controls.Add(this.Label6);
            this.Controls.Add(this.txtUser);
            this.Controls.Add(this.radTrusted);
            this.Controls.Add(this.radSQL);
            this.Controls.Add(this.txtDB);
            this.Controls.Add(this.Label4);
            this.Controls.Add(this.PictureBox1);
            this.Controls.Add(this.Label3);
            this.Controls.Add(this.txtServer);
            this.Name = "DatabaseConn";
            this.Text = "Database Connection";
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.PictureBox PictureBox1;
        internal System.Windows.Forms.Label Label3;
        internal System.Windows.Forms.TextBox txtServer;
        internal System.Windows.Forms.TextBox txtDB;
        internal System.Windows.Forms.Label Label4;
        internal System.Windows.Forms.RadioButton radTrusted;
        internal System.Windows.Forms.RadioButton radSQL;
        internal System.Windows.Forms.Button btnCancel;
        internal System.Windows.Forms.Label Label1;
        internal System.Windows.Forms.Button btnTest;
        internal System.Windows.Forms.TextBox txtPwd;
        internal System.Windows.Forms.Label Label6;
        internal System.Windows.Forms.TextBox txtUser;
    }
}